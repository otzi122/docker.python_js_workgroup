# Python JS Workgroup

It only contains docker python and Js container

## Instalation

* Copy the env file ** cp .env.dist .env ** and customize if you need it 

## DCLI Command

Before all, make a new alias in your profile for dcli 
(on Mac nano ~/.bash_profile or nano ~/.zshrc, on Linux nano ~/.bashrc) and add on your token file as shown below

```bash
    alias dcli='docker-compose -f docker-compose.cli.yml run --rm'
```

## Autor ✒️
* **LPinto** *
